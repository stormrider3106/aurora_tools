#! /usr/bin/env python
import argparse

from aurora import db
from aurora.tools import get_race_stats, get_commanders, get_shipyards, get_event_types, parse_eventlog, get_components, \
    get_fleets


def race_overview():
    print(f"# Race Overview")
    print("")
    print(f"**Name**: {race_stats.name}")
    print(f"**Wealth**: {race_stats.wealth:.2f}")
    print(f"**Population**: {race_stats.population:.2f} million")
    print("")
    print("# Colonies")

    for system_name, colonies in race_stats.colonies.items():

        print("")
        print(f"## System: {system_name}")
        print("")

        for colony in colonies:

            if colony.capital:
                colony_overview = (f"**Name**: {colony.name}\n" +
                                   f"**Capital**: {colony.capital}\n" +
                                   f"**Population**: {colony.population:.2f} million")

            else:
                colony_overview = (f"**Name**: {colony.name}\n" +
                                   f"**Population**: {colony.population:.2f} million")

            print(colony_overview)
            print("")
            print(f"---")
            print("")
            print("")

    if args.shipyards:
        shipyard_overview()


def shipyard_overview():
    shipyards = get_shipyards(args.game, args.race)

    print("")
    print(f"# Shipyards of {race_stats.name}")
    print("")

    for shipyard in shipyards:
        print(f"## Shipyard {shipyard.name}")
        print(f"**Location**: {shipyard.location}")
        print(f"**Slipways**: {shipyard.slipways:.0f}")
        print(f"**Capacity**: {shipyard.capacity:.0f}")
        print(f"**Tooled for**: {shipyard.tooled_for}")

        print(f"")


def commander_overview():
    commanders = get_commanders(game_name=args.game, race_name=args.race, start_date=args.startdate, status=args.status,
                                name_filter=args.filter)

    print("")
    print(f"Commanders of {race_stats.name}")
    print(f"==" * 20)
    for commander in commanders:
        print(f"--" * 20)
        print(f"{commander.cmdr_type} {commander.name}")
        print(f"Species: {commander.species}")
        if commander.rank:
            print(f"Rank: {commander.rank.name}")
        else:
            print("Rank: None")

        print(f"Status: {commander.status}")
        print(f"Current Assignment: {commander.assignment}")

        if commander.traits:
            print("")
            print(f"Traits:")

            for trait in commander.traits:
                print(F"- {trait}")

        if commander.medals:
            print("")
            print(f"Achievements of {commander.cmdr_type} {commander.name}")

            for medal in commander.medals:
                print(F"- {medal.name} awarded for {medal.reason}")

        if commander.history:
            print("")
            print(f"History of {commander.cmdr_type} {commander.name}")

            for entry in commander.history:
                print(f"- {entry.time}: {entry.text}")
    print(f"--" * 20)


def fleet_overview():
    fleets = get_fleets(args.game, args.race, args.filter, args.commercial)

    for fleet_name, ships in fleets.items():
        print("")
        print(f"# Fleet {fleet_name}")
        print("")

        for ship in ships:
            print(f"- {ship.name} ({ship.shipclass})")

        print("")
        print("--" * 20)


def event_overview():
    if args.showeventtypes:
        for eventtype in get_event_types():
            print(f"Eventtype:{eventtype.Description};ID:{eventtype.EventTypeID}")
    else:
        parse_eventlog(timeframe=args.time, startdate=args.startdate, game_name=args.game, race_name=args.race,
                       event_filter=args.event)


def component_overview():
    components = get_components(game_name=args.game, obsolete=args.obsolete,
                                name_filter=args.filter)

    print("")
    print(f"Components of {race_stats.name}")
    print(f"==" * 20)

    for component in components:
        print(f"--" * 20)
        print(f"Component {component.name}")
        print(f"Obsolete: {component.obsolete}")

    print(f"==" * 20)


parser = argparse.ArgumentParser(
    formatter_class=argparse.RawTextHelpFormatter
)

parser.add_argument('-f', '--file', default='./ressources/AuroraDB.db', help='location of the Aurora database file')
parser.add_argument('-g', '--game', required=True, help='Name of the Game')
parser.add_argument('-r', '--race', required=True, help='Name of the race')
parser.add_argument('-s', '--startdate', required=True, help='Startdate of the game please use dd-mm-yyyy format')

sub_parser = parser.add_subparsers(dest='cmd')

stats_parser = sub_parser.add_parser('stats', help='display empire statistics')
stats_parser.add_argument('--shipyards', action='store_true', help='display shipyard data')

event_parser = sub_parser.add_parser('event', help='parse eventlog')
event_parser.add_argument('-e', '--event', help='Filter for events.')
event_parser.add_argument('--showeventtypes', action='store_true', help='list all available eventtypes and their IDs')
event_parser.add_argument('-t', '--time',
                          help='Timeframe of events to parse, please use "dd-mm-yyyy dd-mm-yyyy" format')

commander_parser = sub_parser.add_parser('commanders', help='display commanders')
commander_parser.add_argument('--status', help='active, retired; this does not work due to DB stupidity')
commander_parser.add_argument('--filter', help='show only commanders which contain the filter in name')

components_parser = sub_parser.add_parser('components', help='display components')
components_parser.add_argument('--obsolete', action='store_true', help='show obsolete tech')
components_parser.add_argument('--filter', help='show only components which contain the filter in name')
components_parser.add_argument('--type', help='show only components which contain the filter in name')

fleet_parser = sub_parser.add_parser('fleets', help='display fleets')
fleet_parser.add_argument('--filter', help='show only components which contain the filter in name')
fleet_parser.add_argument('--commercial', action='store_true',
                          help='show only components which contain the filter in name')
args = parser.parse_args()

db.connect(args.file)

race_stats = get_race_stats(args.game, args.race)

if args.cmd == 'stats':
    race_overview()
elif args.cmd == 'event':
    event_overview()
elif args.cmd == 'commanders':
    commander_overview()
elif args.cmd == 'components':
    component_overview()
elif args.cmd == 'fleets':
    fleet_overview()
