import time
from datetime import datetime

import sqlalchemy
from sqlalchemy import asc
from sqlalchemy.orm import Query

from aurora import db
from aurora.models import DIMEventType, FCTRace, FCTGame, FCTPopulation, FCTRank, FCTCommander, FCTCommanderHistory, \
    FCTCommanderMedal, FCTRaceMedal, FCTSpecies, FCTRaceSysSurvey, FCTShipyard, FCTShipClass, DIMTraitsList, \
    FCTCommanderTraits, FCTShipDesignComponent, FCTClassComponent, FCTRaceTech, FCTFleet, FCTShip, \
    FCTGroundUnitFormation
from aurora.tools.models import Race, Colony, Rank, Commander, CommanderHistory, Medal, Shipyard, ShipComponent, Ship
from models import FCTGameLog

game = None
race = None
traits = []
medals: list = []


def parse_eventlog(timeframe, startdate, game_name, race_name, event_filter):
    game = get_game(game_name)
    race = get_race(race_name, game.GameID)
    event_types = get_event_types()

    events = get_events(game.GameID, race.RaceID, timeframe, startdate, event_filter)

    for event in events:
        event_type = get_event_type(event_types, event.EventType)

        time = correct_time(startdate, event.Time)
        print(f"""Event: {event_type.Description}
        Message: {event.MessageText}
        Date: {time}""")


def correct_time(startdate: str, oldtime: float):
    starttime_stamp = time.mktime(datetime.strptime(startdate, "%d-%m-%Y").timetuple())

    return datetime.fromtimestamp(starttime_stamp + oldtime).strftime("%Y-%m-%d")


def get_events(game_id, race_id, timeframe=None, gamestart=None, event_filter=None):
    session = db.get_session()

    query = session.query(FCTGameLog).filter(FCTGameLog.GameID == game_id, FCTGameLog.RaceID == race_id)

    if timeframe and gamestart:
        starttime, stoptime = get_times(timeframe, gamestart)

        query = query.filter(FCTGameLog.Time >= starttime, FCTGameLog.Time <= stoptime)

    if event_filter:
        query = query.filter(FCTGameLog.EventType == event_filter)

    return query.order_by(asc(FCTGameLog.LogID)).group_by(FCTGameLog.RaceID, FCTGameLog.Time).all()


def get_times(timeframe: str, gamestart):
    times = timeframe.split(" ")

    startpoint = datetime.timestamp(datetime.strptime(gamestart, "%d-%m-%Y"))

    starttime = datetime.timestamp(datetime.strptime(times[0], "%d-%m-%Y")) - startpoint
    stoptime = datetime.timestamp(datetime.strptime(times[1], "%d-%m-%Y")) - startpoint

    return starttime, stoptime


def get_event_type(event_types, event_id):
    for event_type in event_types:
        if event_type.EventTypeID == event_id:
            return event_type

    return False


def get_event_types():
    session = db.get_session()

    return session.query(DIMEventType).all()


def get_race(race_name: str, game_id: int):
    global race

    session = db.get_session()

    if not race or race.RaceTitle != race_name or (game_id and race.GameID != game_id):

        query: Query = session.query(FCTRace).filter(FCTRace.RaceTitle == race_name, FCTRace.NPR != 1)

        if game_id:
            query = query.filter(FCTRace.GameID == game_id)

        race = query.one()

    return race


def get_game(game_name: str):
    global game

    session = db.get_session()

    if not game or game.GameName != game_name:
        game = session.query(FCTGame).filter_by(GameName=game_name).one()

    return game


def get_system_name(system_id):
    session = db.get_session()

    system_name = session.query(FCTRaceSysSurvey).filter(FCTRaceSysSurvey.GameID == game.GameID,
                                                         FCTRaceSysSurvey.RaceID == race.RaceID,
                                                         FCTRaceSysSurvey.SystemID == system_id).one().Name

    return system_name


def get_race_stats(game_name, race_name):
    session = db.get_session()

    race_stats = Race()
    game = get_game(game_name)
    race = get_race(race_name, game.GameID)

    colonies = session.query(FCTPopulation).filter(FCTPopulation.GameID == game.GameID,
                                                   FCTPopulation.RaceID == race.RaceID,
                                                   FCTPopulation.Population > 0).all()

    race_stats.name = race.RaceTitle
    race_stats.wealth = race.WealthPoints

    for colony in colonies:
        system_name = get_system_name(colony.SystemID)

        if system_name not in race_stats.colonies.keys():
            race_stats.colonies[system_name] = []

        race_stats.colonies[system_name].append(Colony(colony.PopName, colony.Population, colony.Capital))

    return race_stats


def get_rank(game_name, race_name, rank_id):
    session = db.get_session()
    game = get_game(game_name)
    race = get_race(race_name, game.GameID)

    rank = session.query(FCTRank).filter(FCTRank.GameID == game.GameID,
                                         FCTRank.RaceID == race.RaceID,
                                         FCTRank.RankID == rank_id).one()

    return Rank(rank.RankName, rank.RankAbbrev, rank.RankType)


def get_commanders(game_name, race_name, start_date, status=None, name_filter=None):
    session = db.get_session()
    com_list = []

    game = get_game(game_name)
    race = get_race(race_name, game.GameID)

    query = session.query(FCTCommander).filter(FCTCommander.GameID == game.GameID,
                                               FCTCommander.RaceID == race.RaceID)

    if status:
        if status == 'active':
            query = query.filter(FCTCommander.Retired == 0)
        elif status == 'retired':
            query = query.filter(FCTCommander.Retired == 1)

    if name_filter:
        query = query.filter(FCTCommander.Name.like(f'%{name_filter}%'))

    commanders = query.all()

    for commander in commanders:

        if commander.CommandType == 0:
            assignment = "unassigned"
        elif commander.CommandType == 1:
            assignment = f"Commanding Officer {get_ship_name_from_shipid(commander.CommandID)}"
        elif commander.CommandType == 3:
            assignment = f"Planetary Governor {get_location_from_popid(commander.CommandID)}"
        elif commander.CommandType == 5:
            assignment = f"Unit Commander {get_groundunit_from_unitid(commander.CommandID)}"
        elif commander.CommandType == 7:
            assignment = f"Scientist on Project"
        elif commander.CommandType == 8:
            assignment = f"Executive Officer {get_ship_name_from_shipid(commander.CommandID)}"
        elif commander.CommandType == 9:
            assignment = f"Chief Engineer {get_ship_name_from_shipid(commander.CommandID)}"
        elif commander.CommandType == 10:
            assignment = f"Science Officer {get_ship_name_from_shipid(commander.CommandID)}"
        elif commander.CommandType == 11:
            assignment = f"Tactical Officer {get_ship_name_from_shipid(commander.CommandID)}"
        elif commander.CommandType == 12:
            assignment = "Command Position"
        elif commander.CommandType == 17:
            assignment = f"Academy Commandant {get_location_from_popid(commander.CommandID)}"
        else:
            assignment = "unknown"

        history = get_commander_history(commander.CommanderID, start_date)
        species = get_species(commander.SpeciesID)
        medal_list = get_commander_medals(commander.CommanderID)
        traits = get_commander_traits(commander.CommanderID)

        try:
            rank = get_rank(game_name, race_name, commander.RankID)
        except sqlalchemy.orm.exc.NoResultFound:
            rank = None

        com_list.append(Commander(commander.Name,
                                  rank,
                                  history,
                                  medal_list,
                                  species.SpeciesName,
                                  commander.Retired,
                                  traits,
                                  cmdr_type=commander.CommanderType,
                                  assignment=assignment))
    return com_list


def get_ships_of_fleet(fleet_id):
    session = db.get_session()

    ship_names = []

    ship_list = session.query(FCTShip).filter(FCTShip.GameID == game.GameID,
                                              FCTShip.FleetID == fleet_id).all()

    return ship_list


def get_fleets(game_name, race_name, name_filter=None, commercial=False):
    global game, race
    session = db.get_session()

    fleets = {}

    game = get_game(game_name)
    race = get_race(race_name, game.GameID)

    query = session.query(FCTFleet).filter(FCTFleet.GameID == game.GameID,
                                           FCTFleet.RaceID == race.RaceID)

    if name_filter:
        query = query.limit(FCTFleet.FleetName.like(f'%{name_filter}%'))

    if not commercial:
        query = query.limit(FCTFleet.CivilianFunction == 0)

    fleet_list = query.all()

    for fleet in fleet_list:

        if fleet.FleetName not in fleets.keys():
            fleets[fleet.FleetName] = []

        ships = get_ships_of_fleet(fleet.FleetID)

        for ship in ships:

            shipclass = get_shipclass(ship.ShipClassID)

            fleets[fleet.FleetName].append(Ship(name=ship.ShipName, shipclass=shipclass.ClassName))

    return fleets


def get_commander_traits(commander_id):
    session = db.get_session()

    traits = get_traits()

    traits_list = []

    cmdr_trait_list = session.query(FCTCommanderTraits).filter(FCTCommanderTraits.GameID == game.GameID,
                                                               FCTCommanderTraits.CmdrID == commander_id)

    for cmdr_trait in cmdr_trait_list:
        for trait in traits:
            if cmdr_trait.TraitID == trait.TraitID:
                traits_list.append(trait.Name)
                break

    return traits_list


def get_commander_medals(commander_id):
    session = db.get_session()
    medal_list = []

    commander_medals = session.query(FCTCommanderMedal).filter(FCTCommanderMedal.CommanderID == commander_id).all()
    medals = get_medals()

    for c_medal in commander_medals:
        for medal in medals:
            if c_medal.MedalID == medal.MedalID:
                medal_list.append(Medal(medal.MedalName, c_medal.AwardReason))

    return medal_list


def get_traits():
    global traits
    session = db.get_session()

    traits = session.query(DIMTraitsList).all()

    return traits


def get_medals():
    global medals
    session = db.get_session()

    if not medals:
        medals = session.query(FCTRaceMedal).filter(FCTRaceMedal.GameID == game.GameID,
                                                    FCTRaceMedal.RaceID == race.RaceID).all()

    return medals


def get_species(species_id):
    session = db.get_session()

    return session.query(FCTSpecies).filter(FCTSpecies.GameID == game.GameID,
                                            FCTSpecies.SpeciesID == species_id).one()


def get_commander_history(commander_id, start_date):
    session = db.get_session()
    com_history = []

    history = session.query(FCTCommanderHistory).filter(
        FCTCommanderHistory.CommanderID == commander_id).order_by(asc(FCTCommanderHistory.GameTime),
                                                                  asc(FCTCommanderHistory.ch_id)).all()

    for entry in history:
        com_history.append(CommanderHistory(entry.HistoryText, correct_time(start_date, entry.GameTime)))

    return com_history


def get_location_from_popid(pop_id):
    session = db.get_session()

    population = session.query(FCTPopulation).filter(FCTPopulation.GameID == game.GameID,
                                                     FCTPopulation.RaceID == race.RaceID,
                                                     FCTPopulation.PopulationID == pop_id).one()

    location = f"{get_system_name(population.SystemID)} - {population.PopName}"

    return location

def get_groundunit_from_unitid(unit_id):
    session = db.get_session()

    groundunit = session.query(FCTGroundUnitFormation).filter(FCTGroundUnitFormation.GameID == game.GameID,
                                                              FCTGroundUnitFormation.RaceID == race.RaceID,
                                                              FCTGroundUnitFormation.FormationID == unit_id)

    unit = f"{groundunit.Name}: {get_location_from_popid(groundunit.PopulationID)}"

    return unit


def get_ship_name_from_shipid(ship_id):
    session = db.get_session()

    ship = session.query(FCTShip).filter(FCTShip.GameID == game.GameID,
                                         FCTShip.RaceID == race.RaceID,
                                         FCTShip.ShipID == ship_id)

    ship_name = f"{ship.ShipName}"

    return ship_name


def get_shipclass(ship_class_id=None):
    session = db.get_session()

    query = session.query(FCTShipClass).filter(FCTShipClass.GameID == game.GameID,
                                               FCTShipClass.RaceID == race.RaceID)

    if ship_class_id:
        return query.filter(FCTShipClass.ShipClassID == ship_class_id).one()
    else:
        return query.all()


def get_component_ids(shipclass_ids):
    session = db.get_session()

    class_components = []

    for comp in session.query(FCTClassComponent.ComponentID).filter(FCTClassComponent.GameID == game.GameID,
                                                                    FCTClassComponent.ClassID.in_(shipclass_ids)).all():
        class_components.append(comp.ComponentID)

    return class_components


def get_component_status(component_id):
    session = db.get_session()

    racetech = session.query(FCTRaceTech).filter(FCTRaceTech.RaceID == race.RaceID,
                                                 FCTRaceTech.GameID == game.GameID,
                                                 FCTRaceTech.TechID == component_id).one()

    if racetech.Obsolete == 1:
        return True
    else:
        return False


def get_components(game_name, obsolete=None, name_filter=None):
    session = db.get_session()

    game = get_game(game_name)

    component_list = []

    shipclass_ids = []

    for shipclass in get_shipclass():
        shipclass_ids.append(shipclass.ShipClassID)

    component_ids = get_component_ids(shipclass_ids)

    query = session.query(FCTShipDesignComponent).filter(FCTShipDesignComponent.GameID == game.GameID,
                                                         FCTShipDesignComponent.SDComponentID.in_(component_ids))

    if name_filter:
        query = query.filter(FCTShipDesignComponent.Name.like(f'%{name_filter}%'))

    components = query.all()

    for component in components:
        ship_component = ShipComponent(component.Name, get_component_status(component.SDComponentID))
        if obsolete:
            component_list.append(ship_component)
        else:
            if not ship_component.obsolete:
                component_list.append(ship_component)

    return component_list


def get_shipyards(game_name, race_name):
    session = db.get_session()
    shipyard_list = []

    game = get_game(game_name)
    race = get_race(race_name, game.GameID)

    shipyards = session.query(FCTShipyard).filter(FCTShipyard.GameID == game.GameID,
                                                  FCTShipyard.RaceID == race.RaceID).all()

    for shipyard in shipyards:
        location = get_location_from_popid(shipyard.PopulationID)

        if shipyard.BuildClassID != 0:
            ship_class = get_shipclass(shipyard.BuildClassID).ClassName
        else:
            ship_class = "None"

        shipyard_list.append(Shipyard(shipyard.ShipyardName,
                                      location,
                                      shipyard.Capacity,
                                      ship_class,
                                      shipyard.Slipways))
    return shipyard_list
