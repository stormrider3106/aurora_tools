import warnings

from sqlalchemy import create_engine
from sqlalchemy.exc import SAWarning
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import QueuePool

db_path = '../ressources/AuroraDB.db'

engine = None

Session = None

warnings.filterwarnings('ignore', r".*support Decimal objects natively", SAWarning, r'^sqlalchemy\.sql\.sqltypes$')


def connect(filepath: str = None):
    global db_path, engine, Session

    if filepath:
        db_path = filepath

    engine = create_engine(f'sqlite:///{db_path}', echo=False)

    Session = sessionmaker(bind=engine)


def get_session():
    return Session()
