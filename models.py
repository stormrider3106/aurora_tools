# coding: utf-8
from sqlalchemy import Column, Float, Integer, Text, text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class FCTGameLog(Base):
    __tablename__ = 'FCT_GameLog'

    IncrementID = Column(Integer, server_default=text("0"))
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    SMOnly = Column(Integer, server_default=text("NULL"))
    Time = Column(Float, server_default=text("NULL"))
    EventType = Column(Integer, server_default=text("0"))
    MessageText = Column(Text, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("NULL"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    IDType = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("0"))
    LogID = Column(Integer, primary_key=True, unique=True)
